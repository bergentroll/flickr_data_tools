# Changelog

Changelog format hints on [keepchangelog.com](https://keepachangelog.com/en/1.0.0/)

## [0.2.0] - 2024-01-10

### Added

- `pixelfeeder-export` command to backup Pixelfed posts
- Loading Flicker-like `pixelfeeder-exporter` data to upload on a Pixelfed
    instance
- Alternative images and metadata files matching based on filename (mostly for
  `pixelfeeder-exporter` saves)
- GUI: Show empty window while loading immediately after start
- GUI: URL schema validation
- `timeout` option for max HTTP request execution time
- New dataclasses for subsets of Pixelfed and Flickr posts metadata

### Fixed

- Run on Python < 3.10

### Removed

- Content of submodules are not more imported to the `pixelfed` package

## [0.1.2] - 2024-01-04

### Fixed

- Crash on some orientation EXIF tag values
- Potential error in Flickr data reader
- Few typing issues

### Changed

- Requirements allow Pillow 10

## [0.1.1] - 2023-04-24

### Added

- Navigation scroll
- RAM cache for few next and previous image files
- `pixelfeeder-gui --safe-mode` options to disable RAM cache feature
- Handle truncated images
- Delay for keyboard shortcuts events
- `rm` button on the log screen to clear
- Changelog

## [0.1.0] - 2023-04-17

### Added

- General functionality
- First release
- Package on PyPI
